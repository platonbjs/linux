# .bashrc

# Source global definitions
if [ -f /etc/bashrc ]; then
	. /etc/bashrc
fi

# User specific aliases and functions

alias ls='ls --color'
alias ll='ls -l --color'
alias grep='grep --color'
alias sshr='ssh -X -l root'
alias scpr='scp -p -o User=root'
alias q='exit'

#Regular
BLACK='\[\e[0;30m\]'
#Bold
BBLACK='\[\e[1;30m\]'
#Background
BGBLACK='\[\e[40m\]'
RED='\[\e[0;31m\]'
BRED='\[\e[1;31m\]'
BGRED='\[\e[41m\]'
GREEN='\[\e[0;32m\]'
BGREEN='\[\e[1;32m\]'
BGGREEN='\[\e[32m\]'
YELLOW='\[\e[0;33m\]'
BYELLOW='\[\e[1;33m\]'
BGYELLOW='\[\e[33m\]'
BLUE='\[\e[0;34m\]'
BBLUE='\[\e[1;34m\]'
BGBLUE='\[\e[34m\]'
MAGENTA='\[\e[0;35m\]'
BMAGENTA='\[\e[1;35m\]'
BGMAGENTA='\[\e[35m\]'
CYAN='\[\e[0;36m\]'
BCYAN='\[\e[1;36m\]'
BGCYAN='\[\e[36m\]'
WHITE='\[\e[0;37m\]'
BWHITE='\[\e[1;37m\]'
BGWHITE='\[\e[37m\]'
RESET='\[\e[00m\]'
PS1="[$BCYAN\u@\h\`if [ \$? = 0 ]; then echo -e '$BGREEN:)'; else echo -e '$RED:('; fi\` $BBLUE\W$RESET]$ "

# Base 64 En/Decode
decode () {
  echo "$1" | base64 -d ; echo
}

encode () {
  echo "$1" | base64 ; echo
}

# Easy extract
extract () {
  if [ -f $1 ] ; then
      case $1 in
          *.tar.bz2)   tar xvjf $1    ;;
          *.tar.gz)    tar xvzf $1    ;;
          *.bz2)       bunzip2 $1     ;;
          *.rar)       rar x $1       ;;
          *.gz)        gunzip $1      ;;
          *.tar)       tar xvf $1     ;;
          *.tbz2)      tar xvjf $1    ;;
          *.tgz)       tar xvzf $1    ;;
          *.zip)       unzip $1       ;;
          *.Z)         uncompress $1  ;;
          *.7z)        7z x $1        ;;
          *)           echo "don't know how to extract '$1'..." ;;
      esac
  else
      echo "'$1' is not a valid file!"
  fi
}
genpass() {
        local l=$1
        [ "$l" == "" ] && l=16
        tr -dc A-Za-z0-9_ < /dev/urandom | head -c ${l} | xargs | tee -a .pass
}

encrypt(){
  case $1 in
  -e) echo "$2" | openssl enc -base64 -aes-128-cbc -a -salt -pass pass:"$3"
  ;;
  -f) cat "$2" | openssl enc -base64 -aes-128-cbc -a -salt -pass pass:"$3"
  ;;
  -h|--help) echo "Encrypt a file/string with AES-128-CBC w/ Base64 "
             echo "                   encrypt -e <string> <password>"
             echo "                   encrypt -f <file> <password>  "
  ;;
  esac
}


decrypt(){
  case $1 in
  -e) echo "$2" | openssl enc -d -base64 -aes-128-cbc -a -salt -pass pass:"$3"
  ;;
  -f) cat "$2" | openssl enc -d -base64 -aes-128-cbc -a -salt -pass pass:"$3"
  ;;
  -h|--help) echo  "Decrypt a file/string with AES-128-CBC w/ Base64 "
             echo   "                  decrypt -e <string> <password>"
             echo   "                  decrypt -f <file> <password>"
  ;;
  esac
}
